#!/bin/bash
# setup.sh
# Creates symlinks from the homedir to desired dotfiles in ~/dotfiles

DOTFILE_DIR=${HOME}/.dotfiles
FILE_LIST=".bash_aliases .bash_functions .bashrc .bash_security .gitconfig .tmux.conf .vim .vimrc .xinitrc .Xresources .config/git .config/i3 .config/ranger/rc.conf .lock .config/nvim"


echo "${FILE_LIST}"

# -- Confirmation --
echo "This operation will overwrite your old dotfiles. It is suggested that you backup these files."
echo -n "Continue? [y/N] "
read
[[ ! "${REPLY}" =~ (y|Y) ]] && exit 1
echo

# -- Link --
for file in $FILE_LIST; do
    echo "${DOTFILE_DIR}/${file} -> ${HOME}/${file}"
    echo -n "Link ${file}? [y/N] "
    read
    [[ "${REPLY}" =~ (y|Y) ]] && \
        rm -rf "${HOME}/${file}"
        ln -sf "${DOTFILE_DIR}/${file}" "${HOME}/${file}"
    echo
done

# -- Bash History --
# .bash_history should be append only to prevent malicious users from erasing
# history of what they do.
# To actually modify .bash_history, sudo must be used to remove the attribute.
echo -n "Make .bash_history append only? [Y/n] "
read
[[ ! "${REPLY}" =~ (n|N) ]] && \
    sudo chattr +a "${HOME}/.bash_history"
