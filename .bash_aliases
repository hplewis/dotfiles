#!/bin/bash
# .bash_aliases

### Aliases ###
alias rm='rm --verbose --interactive=always --preserve-root'
alias mv='mv --verbose --interactive'
alias cp='cp --interactive'
alias ls='ls --human-readable --color=auto'
alias refresh='. ~/.bashrc'
alias pulldot='PREV_WD=$PWD; cd ~/.dotfiles/; git pull; cd "$PREV_WD"'
alias rmnode='rm -rf node_modules >/dev/null'
alias cbcopy='xsel --clipboard --input'
alias cbpaste='xsel --clipboard --output'


### Conditional Aliases ###
command -v xdg-open >/dev/null 2>&1 && alias open='xdg-open'
command -v gnome-open >/dev/null 2>&1 && alias open='gnome-open'

command -v google-chrome >/dev/null 2>&1 && alias chrome='google-chrome'
command -v google-chrome-stable >/dev/null 2>&1 && alias chrome='google-chrome-stable'
