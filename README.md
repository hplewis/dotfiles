# .dotfiles
A collection of all the dotfiles I use and have configured over time. Provides configuration for bash, vim, git, i3, etc.

## Setup
The ```setup.sh``` file can be used to quickly setup your environment with the dotfiles and keep them updated as they are edited in the repository.

To setup on your machine, use the following steps:

1. Clone the repository to your homedir

```sh
$ cd ~
$ git clone --recursive https://github.com/hplewis/.dotfiles
$ cd ~/.dotfiles/
```

2. Run the setup script and follow on screen prompts.

```sh
$ ./setup.sh
```

The setup script will run your through a few different options, including
backup on previous dotfiles. Once the script is finished, you will have
symlinks to all dotfiles in the repository in your homedir. Editing either of 
the files (in ~/ or ~/.dotfiles/) will result in an update to the file.

## Post Setup

### Neovim

Open a file and run `:PlugInstall` to install plugins. See init.vim for the list of plugins.

### Bash Security

**Important:** If you do not want people to be able to modify your .bash_history in any way other than appending through commands, make the file append only by running ```sudo chattr +x ~/.bash_history``` in your shell.

### Git Config

Be sure to update your name and email address in the ```.gitconfig``` file.

### Note

Do **not** remove the ```~/.dotfiles``` directory. The actual files are located there. If you wish to remove the directory, first manually copy the configuration files you want into your home directory.
