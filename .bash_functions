#!/bin/bash
# .bash_functions


### Functions ###

# Go up $1 number of directory levels. Default 1.
# Params:
#   $1 The number of directory levels to go up
up() {
    local levels
    local i

    # If $1 has a value, use it. Else, use 1.
    [[ -z "$1" ]] && levels=1 || levels="$1"

    for ((i=0; i<levels; i++)); do
        cd ../
    done
}

# Open a pdf using evince in the background and ignore output
# Params:
#   $1 The pdf to open
evincepdf() {
    evince "$1" >/dev/null 2>&1 &
}
