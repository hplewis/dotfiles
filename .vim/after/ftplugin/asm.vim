setlocal shiftwidth=8
setlocal softtabstop=8
setlocal tabstop=8

au BufWinEnter * let w:m2=matchadd('ErrorMsg', '\%>80v.\+', -1) " Highlight any text on a line past column 80
