setlocal shiftwidth=2
setlocal softtabstop=2
setlocal tabstop=2

au BufWinEnter * let w:m2=matchadd('ErrorMsg', '\%>80v.\+', -1) " Highlight any text on a line past column 80
