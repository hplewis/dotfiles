#!/bin/bash

NUMBER="1"

if [ -n "$1" ]; then
    NUMBER=$1
fi

BAT=`acpi -b | grep -E -o '[0-9]+%' | sed -n ${NUMBER}p`

echo  $BAT
echo  $BAT

[ ${BAT%?} -le 10 ] && echo "#FF0000" && exit 0
[ ${BAT%?} -le 30 ] && echo "#FF8000" && exit 0
