#!/bin/bash
# .bashrc

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# Source global definitions
if [ -f /etc/bashrc ]; then
    . /etc/bashrc
fi

# Source security settings from file
if [ -f ~/.bash_security ]; then
    . ~/.bash_security
fi

# Source bash aliases from file
if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# Source bash functions from file
if [ -f ~/.bash_functions ]; then
    . ~/.bash_functions
fi

# Git autocompletion
# https://github.com/git/git/blob/master/contrib/completion/git-completion.bash
if [ -f ~/.git-completion.bash ]; then
    . ~/.git-completion.bash
fi

# Colors
LS_COLORS='di=0;35' ; export LS_COLORS 
if [ -f ~/.dir_colors ]; then
    eval "$(dircolors ~/.dir_colors)"
fi

function retstat {
    echo $?
}

# Change the bash prompt
# user@host dir retval$
if [ -n "$BASH" ]; then
    export PS1='\[\033[01;32m\]\u@\h \[\033[00;31m\]\W $(retstat)\$ \[\033[00m\]'
fi

# Set vim as default editor.
export EDITOR='vim'

# Use vi bash
set -o vi

# Prompt before shell exit.
export IGNOREEOF=1

# Include personal bin in PATH.
export PATH="~/bin:$PATH"

# Disable messages sent to the terminal
if [ -z "$SSH_CLIENT" ] || [ -z "$SSH_TTY" ]; then
    mesg n
fi

# Source machine specific bashrc
if [ -f ~/.bash_local ]; then
    . ~/.bash_local
fi
